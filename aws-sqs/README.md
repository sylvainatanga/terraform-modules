# AWS SQS Terraform module

Terraform module which creates SQS resources on AWS.

## Usage

To run this example you need to execute:

```bash
$ terraform init
$ terraform plan
$ terraform apply
```

Note that this example may create resources which cost money. Run `terraform destroy` when you don't need these resources.


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 5.6 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 5.6 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_sqs_queue.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sqs_queue) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_queues"></a> [queues](#input\_queues) | A map of queues | <pre>map(object({<br>    content_based_deduplication       = optional(bool, null)<br>    deduplication_scope               = optional(string, null)<br>    delay_seconds                     = optional(number, null)<br>    fifo_queue                        = optional(bool, false)<br>    fifo_throughput_limit             = optional(string, null)<br>    kms_data_key_reuse_period_seconds = optional(number, null)<br>    kms_master_key_id                 = optional(string, null)<br>    max_message_size                  = optional(string, null)<br>    message_retention_seconds         = optional(number, null)<br>    receive_wait_time_seconds         = optional(number, null)<br>    sqs_managed_sse_enabled           = optional(bool, true)<br>    visibility_timeout_seconds        = optional(number, null)<br>    tags                              = optional(map(string), {})<br>  }))</pre> | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_arn"></a> [arn](#output\_arn) | ARNs of security groups |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
