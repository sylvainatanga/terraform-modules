output "queue_arns" {
  description = "ARN of SQS Queue"
  value       = module.example_sqs_queue.arn
}
