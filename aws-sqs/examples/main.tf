
# Create SQS queue using terraform module
module "example_sqs_queue" {
  source = "../"

  # Map of SQS queues
  queues = {
    # Create High-throughput FIFO queue
    high-throughput-queue-1 = {
      fifo_queue            = true
      deduplication_scope   = "messageGroup"
      fifo_throughput_limit = "perMessageGroupId"
      tags = {
        "CreatedBy" : "terraform"
        "QueueType" : "high-throughput-queue"
      }
    }
    # Create FIFO queue
    fifo-queue-2 = {
      fifo_queue                  = true
      content_based_deduplication = true
      tags = {
        "CreatedBy" : "terraform"
        "QueueType" : "fifo-queue"
      }
    }
    # Create high throughput fifo queue with retention and max size
    custom-queue-3 = {
      content_based_deduplication = true
      deduplication_scope         = "messageGroup"
      delay_seconds               = 90
      fifo_queue                  = true
      fifo_throughput_limit       = "perMessageGroupId"
      max_message_size            = 2048
      message_retention_seconds   = 86400
      receive_wait_time_seconds   = 10
      tags = {
        "CreatedBy" : "terraform"
        "QueueType" : "custom-queue"
      }
    }
    # Create SQS queue with Server-side encryption (SSE)
    sse-queue-4 = {
      sqs_managed_sse_enabled = true
      tags = {
        "CreatedBy" : "terraform"
        "QueueType" : "sse-queue"
      }
    }
    # Create SQS queue with KMS key encryption
    kms-queue-5 = {
      kms_master_key_id                 = "alias/aws/sqs"
      kms_data_key_reuse_period_seconds = 300
      tags = {
        "CreatedBy" : "terraform"
        "QueueType" : "kms-queue"
      }
    }
  }
}
