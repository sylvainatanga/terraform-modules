output "arn" {
  description = "ARNs of security groups"
  value       = { for k, queue in aws_sqs_queue.this : k => queue.arn }
}
