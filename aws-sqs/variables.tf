variable "queues" {
  description = "A map of queues"
  type = map(object({
    content_based_deduplication       = optional(bool, null)
    deduplication_scope               = optional(string, null)
    delay_seconds                     = optional(number, null)
    fifo_queue                        = optional(bool, false)
    fifo_throughput_limit             = optional(string, null)
    kms_data_key_reuse_period_seconds = optional(number, null)
    kms_master_key_id                 = optional(string, null)
    max_message_size                  = optional(string, null)
    message_retention_seconds         = optional(number, null)
    receive_wait_time_seconds         = optional(number, null)
    sqs_managed_sse_enabled           = optional(bool, true)
    visibility_timeout_seconds        = optional(number, null)
    tags                              = optional(map(string), {})
  }))
}
