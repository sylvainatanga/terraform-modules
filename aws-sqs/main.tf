resource "aws_sqs_queue" "this" {
  for_each = { for k, v in var.queues : k => v }

  name                              = each.value.fifo_queue ? "${each.key}.fifo" : each.key
  content_based_deduplication       = each.value.content_based_deduplication
  deduplication_scope               = each.value.deduplication_scope
  delay_seconds                     = each.value.delay_seconds
  fifo_queue                        = each.value.fifo_queue
  fifo_throughput_limit             = each.value.fifo_throughput_limit
  kms_data_key_reuse_period_seconds = each.value.kms_data_key_reuse_period_seconds
  kms_master_key_id                 = each.value.kms_master_key_id
  max_message_size                  = each.value.max_message_size
  message_retention_seconds         = each.value.message_retention_seconds
  receive_wait_time_seconds         = each.value.receive_wait_time_seconds
  sqs_managed_sse_enabled           = each.value.kms_master_key_id != null ? null : each.value.sqs_managed_sse_enabled
  visibility_timeout_seconds        = each.value.visibility_timeout_seconds
  tags                              = each.value.tags
}
