output "cloudwatch_log_group_arn" {
  description = "ARNs of Cloudwatch log groups"
  value       = { for k, log_group in aws_cloudwatch_log_group.this : k => log_group.arn }
}
