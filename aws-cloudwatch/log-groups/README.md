# Terraform Cloudwatch Log Groups

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 5.6 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 5.6 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_log_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_log_groups"></a> [log\_groups](#input\_log\_groups) | List of log groups | <pre>map(object({<br>    retention_in_days = optional(number, null)<br>    kms_key_id        = optional(string, null)<br>    log_group_class   = optional(string, null)<br>    skip_destroy      = optional(bool, false)<br>    tags              = optional(map(string), {})<br>  }))</pre> | <pre>{<br>  "default-log-group-1": {<br>    "retention_in_days": 3,<br>    "skip_destroy": false<br>  },<br>  "default-log-group-2": {<br>    "retention_in_days": 7,<br>    "skip_destroy": false<br>  }<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cloudwatch_log_group_arn"></a> [cloudwatch\_log\_group\_arn](#output\_cloudwatch\_log\_group\_arn) | ARNs of Cloudwatch log groups |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
