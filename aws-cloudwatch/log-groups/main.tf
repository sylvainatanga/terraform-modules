resource "aws_cloudwatch_log_group" "this" {
  for_each = { for k, v in var.log_groups : k => v }

  name              = each.key
  kms_key_id        = each.value.kms_key_id
  retention_in_days = each.value.retention_in_days
  log_group_class   = each.value.log_group_class
  skip_destroy      = each.value.skip_destroy
  tags              = each.value.tags
}
