variable "log_groups" {
  description = "List of log groups"
  type = map(object({
    retention_in_days = optional(number, null)
    kms_key_id        = optional(string, null)
    log_group_class   = optional(string, null)
    skip_destroy      = optional(bool, false)
    tags              = optional(map(string), {})
  }))

  # validate retention values
  validation {
    condition = alltrue([for o in var.log_groups : (
    o.retention_in_days == null ? true : contains([0, 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653], o.retention_in_days))])
    error_message = "Must be 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653 or 0 (zero indicates never expire logs)."
  }

  # defaults
  default = {
    default-log-group-1 = {
      retention_in_days = 3
      skip_destroy      = false
    }
    default-log-group-2 = {
      retention_in_days = 7
      skip_destroy      = false
    }
  }
}
