module "example_log_group" {
  source = "../"

  # Map of log groups
  log_groups = {
    # log group 1
    example-log-group-1 = {
      retention_in_days = 3
      skip_destroy      = false
      tags = {
        CreatedBy = "terraform"
      }
    }
    # log group 1
    example-log-group-2 = {
      retention_in_days = 7
      skip_destroy      = false
      tags = {
        CreatedBy = "terraform"
      }
    }
  }

}