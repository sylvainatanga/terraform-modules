output "arn" {
  description = "ARN of the log group"
  value       = module.example_log_group.cloudwatch_log_group_arn
}