resource "aws_iam_role" "this" {
  for_each = { for k, v in var.roles : k => v }

  name                  = each.key
  description           = each.value.description
  max_session_duration  = each.value.max_session_duration
  path                  = each.value.path
  assume_role_policy    = each.value.assume_role_policy
  permissions_boundary  = each.value.permissions_boundary
  force_detach_policies = each.value.force_detach_policies
  managed_policy_arns   = each.value.managed_policy_arns
  tags                  = each.value.tags
}
