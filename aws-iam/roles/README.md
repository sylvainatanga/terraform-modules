# Terraform IAM Role module

This terraform modules creates IAM Roles.

## Usage

To run this example you need to execute:

```bash
$ terraform init
$ terraform plan
$ terraform apply
```

Note that this example may create resources which cost money. Run `terraform destroy` when you don't need these resources.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 5.6 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 5.6 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_role.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_roles"></a> [roles](#input\_roles) | List of roles | <pre>map(object({<br>    description          = optional(string, null)<br>    max_session_duration = optional(number, 3600)<br>    path                 = optional(string, "/")<br>    # assume_role_policy required always<br>    assume_role_policy    = string<br>    permissions_boundary  = optional(string, null)<br>    force_detach_policies = optional(bool, false)<br>    managed_policy_arns   = optional(list(string), [])<br>    tags                  = optional(map(string), {})<br>  }))</pre> | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_role_arn"></a> [role\_arn](#output\_role\_arn) | ARN of Roles |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
