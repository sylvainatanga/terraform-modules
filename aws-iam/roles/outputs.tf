output "role_arn" {
  description = "ARN of Roles"
  value       = { for k, role in aws_iam_role.this : k => role.arn }
}
