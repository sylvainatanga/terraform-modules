variable "roles" {
  description = "List of roles"
  type = map(object({
    description          = optional(string, null)
    max_session_duration = optional(number, 3600)
    path                 = optional(string, "/")
    # assume_role_policy required always
    assume_role_policy    = string
    permissions_boundary  = optional(string, null)
    force_detach_policies = optional(bool, false)
    managed_policy_arns   = optional(list(string), [])
    tags                  = optional(map(string), {})
  }))
}
