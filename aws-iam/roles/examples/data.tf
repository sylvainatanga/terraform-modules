locals {
  aws_account_id = data.aws_caller_identity.current.account_id
  partition      = data.aws_partition.current.partition
}

data "aws_caller_identity" "current" {}
data "aws_partition" "current" {}

data "aws_iam_policy_document" "instance_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "oidc_assume_role_policy" {
  # Allow assume role access to particular iam role arn
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    condition {
      test     = "ArnLike"
      variable = "aws:PrincipalArn"
      values   = ["arn:${local.partition}:iam::${local.aws_account_id}:role/test-role"]
    }
  }
  # Allow OIDC provider access
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]
    principals {
      type        = "Federated"
      identifiers = ["arn:${data.aws_partition.current.partition}:iam::${local.aws_account_id}:oidc-provider/gitlab-account-oidc"]
    }
    condition {
      test     = "StringEquals"
      variable = "demo-gitlab-account:sub"
      values   = ["terraform-modules-repo"]
    }
  }
}

data "aws_iam_policy_document" "mfa_assume_role_policy" {
  # Allow MFA login only
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    condition {
      test     = "Bool"
      variable = "aws:MultiFactorAuthPresent"
      values   = ["true"]
    }

    condition {
      test     = "NumericLessThan"
      variable = "aws:MultiFactorAuthAge"
      values   = ["3600"]
    }
  }
}

data "aws_iam_policy_document" "trusted_assume_role_policy" {
  # Allow trusterd roles and services to be assumed without conditions
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    # Allow access to other AWS account users
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    # Allow access to different services
    principals {
      type = "Service"
      identifiers = [
        "codedeploy.amazonaws.com",
        "ec2.amazonaws.com"
      ]
    }
  }
}

data "aws_iam_policy_document" "self_admin_assume_role_policy" {
  # Assume to an Admin role
  statement {
    sid     = "ExplicitSelfAdminRoleAssumption"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    condition {
      test     = "ArnLike"
      variable = "aws:PrincipalArn"
      values   = ["arn:${local.partition}:iam::${local.aws_account_id}:role/AdminRole"]
    }
  }
}
