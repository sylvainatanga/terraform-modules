locals {
  # Define custom managed IAM policies
  ec2_readonly = {
    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["ec2:Describe*"]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
  }) }
  s3_readonly = {
    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["s3:ListAllMyBuckets", "s3:ListBucket", "s3:HeadBucket"]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
  }) }
  tags = {
    "CreatedBy" = "terraform"
  }
}

# Use policies modules to create managed policies
module "policies" {
  source = "../../policies"

  policies = {
    ec2_readonly = local.ec2_readonly
    s3_readonly  = local.s3_readonly
  }
}

# Create iam roles
module "example_roles" {
  source = "../"

  roles = {
    # Create instance iam assume role with managed policies and assume role via policies module
    test1_role = {
      description         = "allow ec2 access role"
      assume_role_policy  = data.aws_iam_policy_document.instance_assume_role_policy.json
      managed_policy_arns = [module.policies.policy_arn.ec2_readonly, module.policies.policy_arn.s3_readonly]
      tags                = local.tags
    }
    # Create instance iam assume role using default managed policies and aws_iam_policy_document data source
    test2_role = {
      description        = "all s3 access role"
      assume_role_policy = data.aws_iam_policy_document.instance_assume_role_policy.json
      # https://docs.aws.amazon.com/aws-managed-policy/latest/reference/policy-list.html
      managed_policy_arns = ["arn:aws:iam::aws:policy/AmazonS3FullAccess", "arn:aws:iam::aws:policy/AmazonEC2FullAccess"]
      tags                = local.tags
    }
    # Create oidc assume role
    test_oidc_assume_role = {
      description        = "oidc_assume_role"
      assume_role_policy = data.aws_iam_policy_document.oidc_assume_role_policy.json
      tags               = local.tags
    }
    # Create mfa assume role
    test_mfa_assume_role = {
      description        = "mfa_assume_role_policy"
      assume_role_policy = data.aws_iam_policy_document.mfa_assume_role_policy.json
      tags               = local.tags
    }
    # Create trusted roles and services assume role
    test_trusted_assume_role = {
      description        = "trusted_assume_role_policy"
      assume_role_policy = data.aws_iam_policy_document.trusted_assume_role_policy.json
      tags               = local.tags
    }
    # Create self admin assume role
    test_self_admin_assume_role = {
      description        = "self_admin_assume_role_policy"
      assume_role_policy = data.aws_iam_policy_document.self_admin_assume_role_policy.json
      tags               = local.tags
    }
  }
}
