# AWS IAM Policy Example

Terraform module which creates AWS IAM Policy resources.

## Usage

To run this example you need to execute:

```bash
$ terraform init
$ terraform plan
$ terraform apply
```

Note that this example may create resources which cost money. Run `terraform destroy` when you don't need these resources.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_multiple_policy_example"></a> [multiple\_policy\_example](#module\_multiple\_policy\_example) | ../ | n/a |
| <a name="module_single_policy_example"></a> [single\_policy\_example](#module\_single\_policy\_example) | ../ | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy_document.ec2_readonly](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.iam_admin](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

No inputs.

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_multiple_policy_arn"></a> [multiple\_policy\_arn](#output\_multiple\_policy\_arn) | ARN of Policies |
| <a name="output_single_policy_arn"></a> [single\_policy\_arn](#output\_single\_policy\_arn) | ARN of Policies |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
