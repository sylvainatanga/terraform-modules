output "single_policy_arn" {
  description = "ARN of Policies"
  value       = module.single_policy_example.policy_arn
}

output "multiple_policy_arn" {
  description = "ARN of Policies"
  value       = module.multiple_policy_example.policy_arn
}
