provider "aws" {
  region = "us-west-2"
}

data "aws_iam_policy_document" "ec2_readonly" {
  statement {
    sid       = "AllowEc2ReadOnly"
    actions   = ["ec2:List*", "ec2:Describe*"]
    resources = ["*"]
  }
}

data "aws_iam_policy_document" "iam_admin" {
  statement {
    sid       = "AllowIamAdmin"
    actions   = ["iam:*"]
    resources = ["*"]
  }
}

#########################################
# IAM policies
#########################################

# Create single policy
module "single_policy_example" {
  source = "../"

  # Map of policies
  policies = {
    # Policy no.1
    "DeveloperAccess" = {
      path        = "/"
      description = "Developer access polcy"
      policy      = data.aws_iam_policy_document.iam_admin.json
      tags = {
        CreatedBy = "terraform"
      }
    }
  }
}

# Create multiple policies
module "multiple_policy_example" {
  source = "../"

  # Map of policies
  policies = {
    # Policy no.1
    "SeniorDeveloperAccess" = {
      path        = "/"
      description = "Developer access polcy"
      policy      = data.aws_iam_policy_document.ec2_readonly.json
      tags = {
        CreatedBy = "terraform"
      }
    }
    # Policy no.2
    "AdminAccess" = {
      path        = "/"
      description = "Admin access polcy"
      policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:*",
        "iam:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
      tags = {
        CreatedBy = "terraform"
      }
    }
  }
}
