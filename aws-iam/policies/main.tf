resource "aws_iam_policy" "this" {
  for_each    = { for k, v in var.policies : k => v }
  name        = each.key
  path        = each.value.path
  description = each.value.description
  policy      = each.value.policy
  tags        = each.value.tags
}
