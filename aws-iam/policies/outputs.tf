output "policy_arn" {
  description = "ARN of Policies"
  value       = { for k, policy in aws_iam_policy.this : k => policy.arn }
}
