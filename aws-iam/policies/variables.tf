variable "policies" {
  description = "List of policies"
  type = map(object({
    description = optional(string, null)
    path        = optional(string, "/")
    policy      = optional(string, null)
    tags        = optional(map(string), {})
  }))
}
