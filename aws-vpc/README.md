# AWS VPC Terraform module

Terraform module which creates VPC resources on AWS.


We will use directly the (Official Community AWS VPC module)[https://github.com/terraform-aws-modules/terraform-aws-vpc]


Look for `examples` dir that shows example of using community module.
