resource "aws_ssm_parameter" "vpc_id" {
  name  = "/vpc/${var.vpc_name}/vpc_id"
  type  = "String"
  value = module.vpc.vpc_id
}

resource "aws_ssm_parameter" "vpc_cidr_block" {
  name  = "/vpc/${var.vpc_name}/vpc_cidr_block"
  type  = "String"
  value = module.vpc.vpc_cidr_block
}

resource "aws_ssm_parameter" "vpc_public_subnets_ids" {
  name  = "/vpc/${var.vpc_name}/public_subnets_ids"
  type  = "StringList"
  value = join(",", module.vpc.public_subnets)
}

resource "aws_ssm_parameter" "vpc_public_subnets_cidr_blocks" {
  name  = "/vpc/${var.vpc_name}/public_subnets_cidr_blocks"
  type  = "StringList"
  value = join(",", module.vpc.public_subnets_cidr_blocks)
}

resource "aws_ssm_parameter" "vpc_private_subnets_ids" {
  name  = "/vpc/${var.vpc_name}/private_subnets_ids"
  type  = "StringList"
  value = join(",", module.vpc.private_subnets)
}

resource "aws_ssm_parameter" "vpc_private_subnets_cidr_blocks" {
  name  = "/vpc/${var.vpc_name}/private_subnets_cidr_blocks"
  type  = "StringList"
  value = join(",", module.vpc.private_subnets_cidr_blocks)
}
