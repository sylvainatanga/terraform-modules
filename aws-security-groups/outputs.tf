output "security_group_id" {
  description = "ARNs of security groups"
  value       = { for k, sg_group in aws_security_group.this : k => sg_group.id }
}
