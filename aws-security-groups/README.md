# AWS Security Group Terraform module

Terraform module which creates AWS Security Group resources.

## Usage

To run this example you need to execute:

```bash
$ terraform init
$ terraform plan
$ terraform apply
```

Note that this example may create resources which cost money. Run `terraform destroy` when you don't need these resources.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 5.6 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 5.6 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_security_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_security_groups"></a> [security\_groups](#input\_security\_groups) | A map of security groups to create | <pre>map(object({<br>    description = string<br>    vpc_id      = string<br>    ingress_rules = list(object({<br>      from_port   = number<br>      to_port     = number<br>      protocol    = string<br>      cidr_blocks = list(string)<br>      description = string<br>    }))<br>    egress_rules = list(object({<br>      from_port   = number<br>      to_port     = number<br>      protocol    = string<br>      cidr_blocks = list(string)<br>      description = string<br>    }))<br>  }))</pre> | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_security_group_id"></a> [security\_group\_id](#output\_security\_group\_id) | ARNs of security groups |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
