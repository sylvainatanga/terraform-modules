# Create single policy
module "security_groups" {
  source = "../"

  # Map of security groups
  security_groups = {
    # security group 1
    example_security_group_1 = {
      description = "Example Security Group 1"
      vpc_id      = "vpc-07eff74690fcae4ae"
      ingress_rules = [
        {
          from_port   = 80
          to_port     = 80
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
          description = "Allow inbound HTTP traffic on any port"
        },
        {
          from_port   = 443
          to_port     = 443
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
          description = "Allow inbound HTTPS traffic on any port"
        },
      ]
      egress_rules = [
        {
          from_port   = 0
          to_port     = 0
          protocol    = "all"
          cidr_blocks = ["0.0.0.0/0"]
          description = "Allow outbound traffic"
        }
      ]
    }
    # security group 2
    example_security_group_2 = {
      description = "Example Security Group 2"
      vpc_id      = "vpc-07eff74690fcae4ae"
      ingress_rules = [
        {
          from_port   = 8080
          to_port     = 8080
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
          description = "Allow inbound 8080 port traffic"
        },
        {
          from_port   = 80
          to_port     = 80
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
          description = "Allow inbound 80 port traffic"
        },
      ]
      egress_rules = [
        {
          from_port   = 0
          to_port     = 0
          protocol    = "all"
          cidr_blocks = ["0.0.0.0/0"]
          description = "Allow outbound traffic"
        }
      ]
    }
  }
}
