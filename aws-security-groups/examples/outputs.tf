output "security_group_ids" {
  description = "ARN of Security Groups IDs"
  value       = module.security_groups.security_group_id
}
