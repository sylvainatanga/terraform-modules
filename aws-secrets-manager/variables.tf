variable "secrets" {
  description = "A map of secrets to create"
  type = map(object({
    description                    = optional(string, null)
    force_overwrite_replica_secret = optional(bool, null)
    kms_key_id                     = optional(string, null)
    recovery_window_in_days        = optional(number, null)
    tags                           = optional(map(string), {})
    secret_string                  = optional(string, null)
    secret_binary                  = optional(string, null)
    version_stages                 = optional(list(string), null)
  }))
}
