resource "aws_secretsmanager_secret" "this" {
  for_each = { for k, v in var.secrets : k => v }

  name                           = each.key
  description                    = each.value.description
  force_overwrite_replica_secret = each.value.force_overwrite_replica_secret
  kms_key_id                     = each.value.kms_key_id
  recovery_window_in_days        = each.value.recovery_window_in_days
  tags                           = each.value.tags
}

resource "aws_secretsmanager_secret_version" "this" {
  for_each = { for k, v in var.secrets : k => v }

  secret_id      = aws_secretsmanager_secret.this[each.key].id
  secret_string  = each.value.secret_string
  secret_binary  = each.value.secret_binary
  version_stages = each.value.version_stages
}
