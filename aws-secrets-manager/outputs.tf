output "arn" {
  description = "The ARN of the secret"
  value       = { for k, secret in aws_secretsmanager_secret.this : k => secret.arn }
}
