output "secrets_arn" {
  description = "ARN of Secrets"
  value       = module.secrets.arn
}
