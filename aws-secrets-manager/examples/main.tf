# Create secrets manager secret
module "secrets" {
  source = "../"

  secrets = {
    # Create simple secret and random password
    "demo01-secerts" = {
      description   = "example 1 secrets"
      secret_string = random_password.demo_01.result
      tags = {
        "CreatedBy" = "terraform"
      }
    }
    # Create secret with recovery window and random password
    "demo02-secerts" = {
      description             = "example 2 secrets"
      secret_string           = random_password.demo_02.result
      recovery_window_in_days = "7"
      tags = {
        "CreatedBy" = "terraform"
      }
    }
    # Create secret with key value pair
    "demo03-secerts" = {
      description = "example 3 secrets"
      secret_string = jsonencode({
        key1 = "value1"
      key2 = "value2" })
      recovery_window_in_days = "7"
      tags = {
        "CreatedBy" = "terraform"
      }
    }
    # Create secret with binary secret
    "demo04-secerts" = {
      description             = "example 4 secrets"
      secret_binary           = base64encode("demo4-secret")
      recovery_window_in_days = "7"
      tags = {
        "CreatedBy" = "terraform"
      }
    }
  }
}

resource "random_password" "demo_01" {

  length           = 15
  special          = true
  override_special = true
}

resource "random_password" "demo_02" {

  length           = 15
  special          = true
  override_special = true
}
